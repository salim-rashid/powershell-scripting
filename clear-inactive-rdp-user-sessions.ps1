$Server = Read-Host "Enter the IP address or the DNS name of the RDP server"
$UserSessions = Get-RDUserSession | where -Property SessionState -EQ STATE_DISCONNECTED

if(!$UserSessions) {
     Write-host "There are no inactive RDP user session on the server $Server!" -ForegroundColor Green
}
else
    { Write-Warning "There are inactive RDP user sessions on the server $Server!"
      Write-Output "Below is the list of inactive RDP sessions"
      $UserSessions 
      Write-Output "Starting to close all inactive RDP sessions, all applications will be closed and logout the user from the $Server"        
      $UserSessionIds = Get-RDUserSession | `
      where -Property SessionState -EQ STATE_DISCONNECTED | `
      select -ExpandProperty UnifiedSessionId
      foreach($UserSessionId in $UserSessionIds) {
      Invoke-RDUserLogoff -HostServer $Server -UnifiedSessionID $UserSessionId -Force -Verbose }
      Write-Output "All inactive RDP user sessions have been closed on from the server $Server."
    }





